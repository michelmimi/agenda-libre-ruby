(function() {
  var modulo = function(a, b) { return (+a % (b = +b) + b) % b; };

  $(document).on('turbolinks:load', function() {
    var idx, markerColors;
    markerColors = ['blue', 'red', 'darkred', 'orange', 'green', 'darkgreen', 'purple', 'darkpuple', 'cadetblue'];
    idx = 0;
    $('#map.list').each(function() {
      var controls, map;
      map = L.map('map');
      map.fitBounds([[60, -20], [30, 30]]);
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a>'
      }).addTo(map);
      controls = L.control.layers(null, null, {
        collapsed: false
      }).addTo(map);
      return $('li a', this).each(function() {
        var markerColor, text, url;
        url = $(this).attr('href');
        text = $(this).html();
        markerColor = markerColors[modulo(idx++, markerColors.length)];
        if (location.search && url.indexOf('?') >= 0) {
          url += '&' + location.search.substr(1);
        } else {
          url += location.search;
        }
        return $.getJSON(url, function(json) {
          var layer;
          if (!(json != null ? json.length : void 0)) {
            return;
          }
          layer = L.markerClusterGroup({
            maxClusterRadius: 30
          }).addLayer(L.geoJson(json, {
            pointToLayer: function(feature, latlng) {
              var marker;
              marker = L.AwesomeMarkers.icon({
                prefix: 'fa',
                icon: feature.properties.icon || 'calendar',
                markerColor: markerColor
              });
              return L.marker(latlng, {
                icon: marker
              });
            },
            onEachFeature: function(feature, layer) {
              if (feature.properties && feature.properties.popupContent) {
                return layer.bindPopup(feature.properties.popupContent);
              }
            }
          }));
          map.addLayer(layer);
          controls.addOverlay(layer, text + ' - ' + json.length);
          if ((/maps\//.test(location.href) || /maps.json/.test(url)) && layer.getBounds()._northEast && layer.getBounds()._southWest) {
            return map.fitBounds(layer.getBounds());
          }
        });
      });
    });
    return $('#map.event, #map.orga').each(function() {
      var coord, map, marker, markerColor, url;
      coord = [$(this).data('latitude'), $(this).data('longitude')];
      map = L.map('map').setView([coord[0], coord[1]], 16);
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a>'
      }).addTo(map);
      url = $(this).data('url');
      markerColor = markerColors[modulo(idx++, markerColors.length)];
      if (location.search && url.indexOf('?') >= 0) {
        url += '&' + location.search.substr(1);
      } else {
        url += location.search;
      }
      marker = L.AwesomeMarkers.icon({
        prefix: 'fa',
        icon: 'calendar'
      });
      L.marker([coord[0], coord[1]], {
        icon: marker
      }).addTo(map);
      return $.getJSON(url, function(json) {
        var layer;
        layer = L.markerClusterGroup({
          maxClusterRadius: 30
        }).addLayer(L.geoJson(json, {
          pointToLayer: function(feature, latlng) {
            marker = L.AwesomeMarkers.icon({
              prefix: 'fa',
              icon: feature.properties.icon || 'calendar',
              markerColor: markerColor
            });
            return L.marker(latlng, {
              icon: marker
            });
          },
          onEachFeature: function(feature, layer) {
            if (feature.properties && feature.properties.popupContent) {
              return layer.bindPopup(feature.properties.popupContent);
            }
          }
        }));
        return map.addLayer(layer);
      });
    });
  });

}).call(this);
