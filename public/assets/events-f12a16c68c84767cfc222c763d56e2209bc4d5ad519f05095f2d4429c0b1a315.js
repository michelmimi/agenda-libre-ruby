(function() {
  $(document).on('turbolinks:load', function() {
    $('#event_start_time').change(function() {
      if ($('#event_start_time').val() >= $('#event_end_time').val()) {
        return $('#event_end_time').val($('#event_start_time').val());
      }
    });
    $('#event_end_time').change(function() {
      if ($('#event_start_time').val() >= $('#event_end_time').val()) {
        return $('#event_start_time').val($('#event_end_time').val());
      }
    });
    $('#event_repeat').each(function() {
      if ($(this).val() === '0') {
        $('.field.rule').hide();
      }
      return $(this).change(function() {
        if ($(this).val() > 0) {
          $('.field.rule').show();
          return $('.field.rule input').attr('required', 'required');
        } else {
          $('.field.rule').hide();
          return $('.field.rule input').removeAttr('required');
        }
      });
    });
    return $('#event_tags').each(function() {
      var elt;
      elt = $(this);
      return $.ajax({
        url: '/tags.json'
      }).done(function(data) {
        var tags;
        return tags = jQuery.map(data, function(n) {
          return n[0];
        });
      });
    });
  });

}).call(this);
