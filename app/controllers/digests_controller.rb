# A digest of all events over a period of time
class DigestsController < ApplicationController
  has_scope :region, :locality, :tag
  has_scope :near, type: :hash, using: %i[location distance]
  has_scope :moderated, default: nil, allow_blank: true
  has_scope :period, allow_blank: true, type: :hash, using: %i[year week],
                     default: (
                       lambda do
                         { year: (Time.zone.today + 7.days).year,
                           week: (Time.zone.today + 7.days).cweek }
                       end)

  before_action :set_week, if: -> { params[:period].present? }
  before_action :set_events, only: [:show]

  def show
    render :markdown
  end

  private

  def set_week
    return if params[:period][:week].blank?

    @week = Date.commercial params[:period][:year].to_i,
                            params[:period][:week].to_i
  end

  def set_events
    @week ||= Time.zone.today + 7.days
    @events = apply_scopes Event
  end
end
