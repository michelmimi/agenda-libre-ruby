ActiveAdmin.register_page 'Dashboard' do
  menu priority: 1, label: proc { I18n.t('active_admin.dashboard') }

  content title: proc { I18n.t('active_admin.dashboard') } do
    columns do
      column do
        panel link_to 'Moderateurs', :admin_users do
          ul do
            User.all.map do |user|
              li "#{user.firstname} #{user.lastname} (#{user.login})"
            end
          end
        end
      end

      column do
        panel 'Commentaires' do
          ul do
            ActiveAdmin::Comment.all.map do |comment|
              li link_to(comment.author.email, admin_comment_path(comment))
            end
          end
        end
      end
    end

    panel 'Git' do
      link_to(`git log -n 1 --oneline`,
              '//git.framasoft.org/agenda-libre/agenda-libre-ruby')
    end
  end
end
