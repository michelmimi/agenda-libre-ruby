$(document).on 'turbolinks:load', ->
	return unless navigator.geolocation

	$('a.near-me').click (event) ->
		event.preventDefault()

		# We store the url we're going to
		window.goto = event.target.href

		navigator.geolocation.getCurrentPosition showPosition, (error) ->
			switch error.code
				when error.PERMISSION_DENIED
					$('ul.regions li#near-me').remove()

showPosition = (position) ->
	location.replace window.goto.replace '[me]',
		"[#{position.coords.latitude}, #{position.coords.longitude}]"
