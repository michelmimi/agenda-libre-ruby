# Preview all emails at http://localhost:3000/rails/mailers/moderationorga_mailer
class ModerationorgaMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/moderationorga_mailer/create
  def create
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    ModerationorgaMailer.create Orga.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/moderationorga_mailer/update
  def update
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    orga = Orga.first

    orga.save!
    orga.name += ' et hop!'
    orga.description = orga.description + '
hello world'

    ModerationorgaMailer.update orga
  end

  # Preview this email at http://localhost:3000/rails/mailers/moderationorga_mailer/accept
  def accept
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    ModerationorgaMailer.accept Orga.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/moderationorga_mailer/destroy
  def destroy
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    orga = Orga.last
    ModerationorgaMailer.destroy orga
  end
end
